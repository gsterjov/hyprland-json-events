#include <asm-generic/errno.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <pony.h>
#include "uds.h"


/** Definiton of an ASIO event.
 *
 *  Used to carry user defined data for event notifications.
 */
typedef struct asio_event_t
{
  struct asio_event_t* magic;
  pony_actor_t* owner;  /* owning actor */
  uint32_t msg_id;      /* I/O handler (actor message) */
  int fd;               /* file descriptor */
  uint32_t flags;       /* event filter flags */
  bool noisy;           /* prevents termination? */
  bool readable;        /* is fd readable? */
  bool writeable;       /* is fd writeable? */
  uint64_t nsec;        /* nanoseconds for timers */
} asio_event_t;

/** Create a new event.
 *
 *  An event is noisy, if it should prevent the runtime system from terminating
 *  based on quiescence.
 */
PONY_API asio_event_t* pony_asio_event_create(pony_actor_t* owner, int fd,
  uint32_t flags, uint64_t nsec, bool noisy);

enum
{
  ASIO_DISPOSABLE = 0,
  ASIO_READ  = 1 << 0,
  ASIO_WRITE = 1 << 1,
  ASIO_TIMER = 1 << 2,
  ASIO_SIGNAL = 1 << 3,
  ASIO_ONESHOT = 1 << 8,
  ASIO_DESTROYED = (uint32_t)-1
};


PONY_API int pony_unix_socket_create()
{
    int fd = socket(AF_UNIX, SOCK_STREAM | SOCK_NONBLOCK, 0);
    return fd;
}

PONY_API void pony_unix_socket_close(int fd)
{
    close(fd);
}


PONY_API asio_event_t* pony_unix_socket_connect(pony_actor_t* owner, int fd, const char* path)
{
    struct sockaddr_un addr;
    memset(&addr, 0, sizeof(struct sockaddr_un));
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, path, sizeof(addr.sun_path) - 1);

    int ret = connect(fd, (const struct sockaddr *) &addr, sizeof(addr));

    if ((ret != 0) && (errno != EINPROGRESS))
    {
        pony_unix_socket_close(fd);
        return NULL;
    }

    asio_event_t* ev = pony_asio_event_create(owner, fd, ASIO_READ, 0, true);
    return ev;
}

PONY_API int pony_unix_socket_recv(asio_event_t* ev, char* buf, size_t len)
{
    ssize_t received = recv(ev->fd, buf, len, 0);
    if (received < 0) {
        // the socket doesn't have any data to read
        // so we return a zero and wait for another epoll event
        if (errno == EWOULDBLOCK || errno == EAGAIN)
            return 0;

        // a read or socket error occurred
        pony_error();
    }
    else if (received == 0) {
        // we received an epoll event but didn't read
        // any data so we raise an error since it is unexpected
        pony_error();
    }

    return received;
}
