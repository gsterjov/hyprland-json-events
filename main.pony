use "cli"
use "json"


class Event
  """
  An event from the Hyprland socket
  """
  let name: String val
  let data: String val

  new parse(line: String val)? =>
    """
    Parse the event string from the socket. Hyprland events are
    represented as a `$name>>$data` string. The name is the event
    that occurred and the data is a comma separated list of information
    related to the event.
    For now this does not try to process the event into known structures.
    instead it will just forward the event data as a string
    """
    let components: Array[String] = line.split_by(">>")
    if components.size() != 2 then error end
    name = components(0)?
    data = components(1)?


actor EventStream
  """
  A JSON event stream converting an `Event` to a JSON document
  and sending it asynchronously to the provided output stream
  """
  let _out: OutStream

  new create(out: OutStream) =>
    _out = out

  be write(event: Event val) =>
    """
    Write the event to the output stream as a JSON document.
    This should be seen as a new line separate output like the following:

        { "name": "workspace", data: "2" }
        { "name": "workspace", data: "1" }
    """
    let doc = JsonDoc
    let obj = JsonObject
    obj.data("name") = event.name
    obj.data("data") = event.data
    doc.data = obj
    _out.print(doc.string())

  be close() =>
    """
    Sends a closed socket event to the output stream to allow for
    proper teardown. Looks like:

        { "socket": "closed" }
    """
    _out.print("{\"socket\": \"closed\"}")



class EventNotify is UnixNotify
  """
  Send each line received by the unix socket to the
  provided event stream. This enables asynchronous IO events
  from the pony runtime to be processed by the json actor
  """
  let _stream: EventStream

  new create(stream: EventStream) =>
    """
    Send parsed events to the provided event stream
    """
    _stream = stream

  fun ref received(sock: UnixSocket ref, data: Array[U8] iso) =>
    """
    Called when new data is received on the unix socket
    """
    let payload = String.from_array(consume data)
    let lines: Array[String] = payload.split_by("\n")

    // ignore lines that aren't formatted as expected
    for line in lines.values() do
      try
        let event: Event val = Event.parse(line)?
        _stream.write(event)
      end
    end

  fun ref closed(sock: UnixSocket ref) =>
    """
    Called when the socket is closed. Including when it is closed
    due to an error
    """
    _stream.close()



actor Main
  var _env: Env

  new create(env: Env) =>
    try
      let vars = EnvVars(env.vars)
      let instance = vars("HYPRLAND_INSTANCE_SIGNATURE")?
      let path = "/tmp/hypr/" + instance + "/.socket2.sock"
      let stream = EventStream(env.out)
      let socket = UnixSocket(EventNotify(stream), consume path)
    else
      env.err.print("Failed to connect to the socket")
    end

    _env = env

