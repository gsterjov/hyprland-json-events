interface UnixNotify

  fun ref received(sock: UnixSocket ref, data: Array[U8] iso) =>
    """
    Called when data is received from the unix socket
    """
    None

  fun ref closed(sock: UnixSocket ref) =>
    """
    Called when the unix socket is closed
    """
    None
