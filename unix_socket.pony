use "lib:uds"

use @pony_asio_event_unsubscribe[None](event: AsioEventID)
use @pony_unix_socket_create[I32]()
use @pony_unix_socket_close[None](fd: I32)
use @pony_unix_socket_connect[AsioEventID](
  owner :AsioEventNotify,
  fd: I32,
  path: Pointer[U8] tag)
use @pony_unix_socket_recv[USize](
  ev: AsioEventID,
  buf: Pointer[U8] tag,
  len: USize)?


actor UnixSocket is AsioEventNotify
  """
  Connects to a Unix domain socket and registers it with ASIO events
  in the Pony runtime. This works similarly to TCP and UDP sockets except
  that it connects to a socket via a filesystem path.
  """
  var _notify: UnixNotify
  var _fd: I32
  var _event: AsioEventID
  var _readable: Bool = false
  var _closed: Bool = false
  var _buffer_size: USize
  var _read_buf: Array[U8] iso

  new create(notify: UnixNotify iso, path: String, buffer_size: USize = 1024) =>
    """
    Connect to the provided socket and send any read data to the notifier
    """
    _notify = consume notify
    _buffer_size = buffer_size
    _read_buf = recover Array[U8] .> undefined(buffer_size) end
    _fd = @pony_unix_socket_create()
    _event = @pony_unix_socket_connect(this, _fd, path.cstring())

  be _event_notify(event: AsioEventID, flags: U32, arg: U32) =>
    if AsioEvent.readable(flags) then
      _readable = true
      _pending_reads()
    end

  be _read_again() =>
    if not _closed then
      _pending_reads()
    end

  fun ref _pending_reads() =>
    try
      var sum: USize = 0

      while _readable do
        let size = _buffer_size
        let data = _read_buf = recover Array[U8] .> undefined(size) end

        let len = @pony_unix_socket_recv(_event, data.cpointer(), data.space())?

        if len == 0 then
          _readable = false
          return
        end

        data.truncate(len)
        _notify.received(this, consume data)

        sum = sum + len
        if sum > (1 << 12) then
          _read_again()
          return
        end
      else
        _close()
      end
    end


  fun ref _close() =>
    if not _event.is_null() then
      @pony_asio_event_unsubscribe(_event)
      _readable = false
    end

    _closed = true

    if _fd != -1 then
      _notify.closed(this)
      @pony_unix_socket_close(_fd)
      _fd = -1
    end
